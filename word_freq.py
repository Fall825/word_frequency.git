def process_file(dst):     # 读文件到缓冲区 
    try:    # 打开文件
        a=open(dst,"r")
    except IOError as s:
        print (s)
        return None
    try:     # 读文件到缓冲区
        bvffer=a.read()
    except:
        print ("Read File Error!")
        return None
    a.close()
    return bvffer
def process_buffer(bvffer):
    if bvffer:
        word_freq = {}
        bvffer=bvffer.lower()
        for b in '"?!;.,':
         # 下面添加处理缓冲区 bvffer代码，统计每个单词的频率，存放在字典word_freq
            bvffer=bvffer.replace(b, " ")
        words=bvffer.strip().split()
        for word in words:
            word_freq[word]=word_freq.get(word,0)+1
        return word_freq
def output_result(word_freq):
    if word_freq:
        sorted_word_freq = sorted(word_freq.items(), key=lambda v: v[1], reverse=True)
        for item in sorted_word_freq[:10]: # 输出 Top 10 的单词
            print(item)
def main():
    dst = "Gone_with_the_wind.txt"
    bvffer = process_file(dst)
    word_freq = process_buffer(bvffer)
    output_result(word_freq)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dst')
    args = parser.parse_args()
    dst = args.dst
    bvffer = process_file(dst)
    word_freq = process_buffer(bvffer)
    output_result(word_freq)
